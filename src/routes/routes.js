const { Router } = require('express');
const ClientController = require('../controllers/ClientController');
const ProductController = require('../controllers/ProductController');
const router = Router();

//rotas para clientes
router.get('/clients',ClientController.index);
router.get('/clients/:id',ClientController.show);
router.post('/clients',ClientController.create);
router.put('/clients/:id', ClientController.update);
router.delete('/clients/:id', ClientController.destroy);

//rotas para produtos
router.get('/products',ProductController.index);
router.get('/products/:id',ProductController.show);
router.post('/products',ProductController.create);
router.put('/products/:id', ProductController.update);
router.delete('/products/:id', ProductController.destroy);
router.put('/products/clientaddbuy/:id', ProductController.addRelationClient);
router.put('/products/clientremovebuy/:id', ProductController.removeRelationClient);

module.exports = router;