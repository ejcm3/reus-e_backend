const {response} = require('express');
const Client = require('../models/Client');
const Product = require('../models/Product');

//cria um cliente
const create = async(req,res)=>{
    try{
        const client = await Client.create(req.body);
        return res.status(201).json({message: "Cliente cadastrado com sucesso!", client: client});
    }catch(err){
        res.status(500).json({error: err});
    }
};

//mostra os clientes
const index = async(req,res)=>{
    try{
        const clients = await Client.findAll();
        return res.status(200).json({clients});
    }catch(err){
        res.status(500).json({err});
    }
};

//mostra um cliente
const show = async(req,res)=>{
    const{id} = req.params;
    try{
        const client = await Client.findByPk(id);
        return res.status(200).json({client});
    }catch(err){
        res.status(500).json({err});
    }
};

//edita um cliente
const update = async(req,res)=>{
    const{id} = req.params;
    try{
        const [updated] = await Client.update (req.body, {where: {id: id}});
        if(updated){
            const client = await Client.findByPk(id);
            return res.status(200).send(client);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Cliente não encontrado");
    }
};

//deleta um cliente
const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Client.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Cliente deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Cliente não encontrado.");
    }
};

module.exports={
    create,
    index,
    show,
    update,
    destroy
};