const {response} = require('express');
const Client = require('../models/Client');
const Product = require('../models/Product');

//cria um produto
const create = async(req,res)=>{
    try{
        const product = await Product.create(req.body);
        return res.status(201).json({message: "Produto cadastrado com sucesso!", Product: Product});
    }catch(err){
        res.status(500).json({error: err});
    }
};

//mostra os produtos
const index = async(req,res)=>{
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err){
        res.status(500).json({err});
    }
};

//mostra um produto
const show = async(req,res)=>{
    const{id} = req.params;
    try{
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }catch(err){
        res.status(500).json({err});
    }
};

//edita um produto
const update = async(req,res)=>{
    const{id} = req.params;
    try{
        const [updated] = await Product.update (req.body, {where: {id: id}});
        if(updated){
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado");
    }
};

//deleta um produto
const destroy = async(req,res)=>{
    const {id} = req.params;
    try{
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted){
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

//adciona o id do cliente ao produto que foi comprado
const addRelationClient = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const client = await Client.findByPk(req.body.ClientId);
        await product.setClient(client);
        return res.status(200).json(client);
    }catch(err){
        return res.status(500).json({err});
    }
};

//remove o id do cliente do produto
const removeRelationClient = async(req,res) => {
    const {id} = req.params;
    console.log(id);
    try {
        const product = await Product.findByPk(id);
        await product.setClient(null);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports={
    create,
    index,
    show,
    update,
    destroy,
    addRelationClient,
    removeRelationClient
};