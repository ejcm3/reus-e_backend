const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product',{
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    conservation:{ //usado/seminovo/novo
        type: DataTypes.STRING,
        allowNull: false
    },
    image:{
        type: DataTypes.STRING,
        allowNull: false
    },
    category:{ //gabinete, ram, hd, ssd etc
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    timestamps: false
});

Product.associate = function(models){
    Product.belongsTo(models.Client);
};

module.exports = Product;